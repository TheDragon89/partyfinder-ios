//
//  Party.h
//  PartyFinder
//
//  Created by Matthew Knippen on 4/23/12.
//  Copyright (c) 2012 Zwiffer Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Party : NSObject

@property (nonatomic, strong) NSString *endTime;
@property (nonatomic) int location_id;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *title;
@property (nonatomic) int user_id;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic) int heroku_id;

- (void)setWithDictionary:(NSDictionary *)dict;

@end
