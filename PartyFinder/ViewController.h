//
//  ViewController.h
//  PartyFinder
//
//  Created by Matthew Knippen on 3/5/12.
//  Copyright (c) 2012 Zwiffer Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <RestKit/RestKit.h>

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate, RKRequestDelegate> {
    
    IBOutlet MKMapView *mapView;
    IBOutlet UITableView *listTableView;
    IBOutlet UISegmentedControl *segmentedControl;
    NSMutableArray *parties;
}

- (IBAction)segmentedControlChanged:(id)sender;

- (void)mapView:(MKMapView *)mapView didChangeUserTrackingMode:(MKUserTrackingMode)mode animated:(BOOL)animated;


@end
