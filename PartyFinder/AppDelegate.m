//
//  AppDelegate.m
//  PartyFinder
//
//  Created by Matthew Knippen on 3/5/12.
//  Copyright (c) 2012 Zwiffer Inc. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"
#import "Location.h"
#import "JSON.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;

- (void) getLocations {
    RKClient *client = [RKClient clientWithBaseURLString:@"http://smooth-galaxy-9706.herokuapp.com/"];
    [client get:@"/locations.json" delegate:self];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    ViewController *vc;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        vc = [[ViewController alloc] initWithNibName:@"ViewController_iPhone" bundle:nil];
    } else {
        vc = [[ViewController alloc] initWithNibName:@"ViewController_iPad" bundle:nil];
    }
    
    self.viewController = [[UINavigationController alloc] initWithRootViewController:vc];
    self.viewController.navigationBar.barStyle = UIBarStyleBlack;
    
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
    [self getLocations];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - RestKit Methods

- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {  
    if ([request isGET]) {
        // Handling GET /foo.xml
        
        if ([response isOK]) {
            // Success! Let's take a look at the data
            NSLog(@"Retrieved JSON: %@", [response bodyAsString]);
            NSString *jsonStr = [response bodyAsString];
            NSArray *jsonArr = [jsonStr JSONValue];
            
//            if (parties && jsonArr && jsonArr.count > 0) {
//                parties = nil;
//            }
//            
//            parties = [[NSMutableArray alloc] initWithCapacity:jsonArr.count];
            
            for (NSDictionary *dict in jsonArr) {
                NSDictionary *locationDict = [dict objectForKey:@"location"];
                NSLog(@"%@", locationDict);
                Location *aLocation = [[Location alloc] init];
                [aLocation setWithDictionary:locationDict];
            }
        } 
    }
}



@end
