//
//  Location.h
//  PartyFinder
//
//  Created by Matthew Knippen on 4/23/12.
//  Copyright (c) 2012 Zwiffer Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Location : NSObject

@property (nonatomic, strong) NSString *address;
@property (nonatomic) int heroku_id;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *latitude;

+ (Location *)locationWithId:(int)heroku_id;

- (void)setWithDictionary:(NSDictionary *)locationDict;


@end
