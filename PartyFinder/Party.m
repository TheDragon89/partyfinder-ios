//
//  Party.m
//  PartyFinder
//
//  Created by Matthew Knippen on 4/23/12.
//  Copyright (c) 2012 Zwiffer Inc. All rights reserved.
//

#import "Party.h"

@implementation Party

@synthesize endTime, location_id, startTime, title, user_id, desc, heroku_id;

- (void)setWithDictionary:(NSDictionary *)dict {
    self.title = [dict objectForKey:@"title"];
    self.desc = [dict objectForKey:@"description"];
    self.heroku_id = [[dict objectForKey:@"id"] intValue];
    self.location_id = [[dict objectForKey:@"location_id"] intValue];
    self.user_id = [[dict objectForKey:@"user_id"] intValue];
    self.startTime = [dict objectForKey:@"start_time"];
    self.endTime = [dict objectForKey:@"end_time"];
}

@end
