//
//  PartyDetailViewController.m
//  PartyFinder
//
//  Created by Matthew Knippen on 4/10/12.
//  Copyright (c) 2012 Zwiffer Inc. All rights reserved.
//

#import "PartyDetailViewController.h"
#import "Party.h"
#import "Location.h"

@interface PartyDetailViewController ()

@end

@implementation PartyDetailViewController

@synthesize nameLabel;
@synthesize descLabel;
@synthesize startTimeLabel;
@synthesize endTimeLabel;
@synthesize addressLabel;
@synthesize party;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Party Details";
    }
    return self;
}

- (void)setupDetails {
    self.nameLabel.text = party.title;
    self.descLabel.text = party.desc;
    self.startTimeLabel.text = [NSString stringWithFormat:@"Start: %@", party.startTime];
    self.endTimeLabel.text = [NSString stringWithFormat:@"End: %@", party.endTime];
    
    Location *loc = [Location locationWithId:party.location_id];
    self.addressLabel.text = loc.address;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupDetails];
}

- (void)viewDidUnload
{
    [self setNameLabel:nil];
    [self setDescLabel:nil];
    [self setStartTimeLabel:nil];
    [self setEndTimeLabel:nil];
    [self setAddressLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
