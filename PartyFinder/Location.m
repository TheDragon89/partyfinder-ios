//
//  Location.m
//  PartyFinder
//
//  Created by Matthew Knippen on 4/23/12.
//  Copyright (c) 2012 Zwiffer Inc. All rights reserved.
//

#import "Location.h"

@implementation Location

@synthesize address, heroku_id, longitude, latitude;

static NSMutableDictionary *locations = nil;

+ (Location *)locationWithId:(int)heroku_id {
    if (locations == nil) {
        return nil;
    }
    
    return [locations objectForKey:[NSNumber numberWithInt:heroku_id]];
    
}

+ (void)addLocation:(Location *)loc {
    if (locations == nil) {
        locations = [[NSMutableDictionary alloc] initWithCapacity:30];
    }
    
    [locations setObject:loc forKey:[NSNumber numberWithInt:loc.heroku_id]];
    
}


- (void)setWithDictionary:(NSDictionary *)locationDict {
    self.address = [locationDict objectForKey:@"address"];
    self.latitude = [locationDict objectForKey:@"latitude"];
    self.longitude = [locationDict objectForKey:@"address"];
    self.heroku_id = [[locationDict objectForKey:@"id"] intValue];
    [Location addLocation:self];
}

@end
