//
//  ViewController.m
//  PartyFinder
//
//  Created by Matthew Knippen on 3/5/12.
//  Copyright (c) 2012 Zwiffer Inc. All rights reserved.
//

#import "ViewController.h"
#import "PartyDetailViewController.h"
#import "AddPartyViewController.h"
#import "JSON.h"
#import "Party.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    mapView.hidden = YES;
    self.title = @"PartyFinder";
    self.navigationController.navigationBar.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addButtonPressed)];
	// Do any additional setup after loading the view, typically from a nib.
   // RKClient *client = [RKClient clientWithBaseURLString:@"http://smooth-galaxy-9706.herokuapp.com/"];
    
   // [client get:@"/parties.json" delegate:self];
    
}

- (void)viewDidUnload
{
    mapView = nil;
    listTableView = nil;
    segmentedControl = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

- (void)getPartiesWithCoordinate:(CLLocationCoordinate2D)coord {
    double latit = coord.latitude *100000000;
    double longit = coord.longitude *100000000;
    
    NSLog(@"lat: %f long: %f", coord.latitude, coord.longitude);
    NSLog(@"lat: %f long: %f", latit, longit);

    NSString *latStr = [NSString stringWithFormat:@"%f", latit];
    latStr = [[latStr componentsSeparatedByString:@"."] objectAtIndex:0];
    NSString *longStr = [NSString stringWithFormat:@"%f", longit];
    longStr = [[longStr componentsSeparatedByString:@"."] objectAtIndex:0];
    NSLog(@"lat: %@ long: %@", latStr, longStr);
    
    NSString *getStr = [NSString stringWithFormat:@"/find_party/%@/%@.json", latStr, longStr];
    [[RKClient sharedClient] get:getStr delegate:self];

}


#pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (parties) {
        return parties.count;
    } else {
        return 0;
    }
    
    
    //return 5;   //for testing
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    Party *p = [parties objectAtIndex:indexPath.row];
    
    cell.textLabel.text = p.title;
    cell.detailTextLabel.text = p.desc;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PartyDetailViewController *vc = [[PartyDetailViewController alloc] initWithNibName:@"PartyDetailViewController" bundle:nil];
    //send party info
    vc.party = [parties objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - MKMapView Methods

- (void)mapView:(MKMapView *)mapView didChangeUserTrackingMode:(MKUserTrackingMode)mode animated:(BOOL)animated {
    
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    [self getPartiesWithCoordinate:userLocation.location.coordinate];
}


#pragma mark - RestKit Methods

- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {  
    if ([request isGET]) {
        // Handling GET /foo.xml
        
        if ([response isOK]) {
            // Success! Let's take a look at the data
            NSLog(@"Retrieved JSON: %@", [response bodyAsString]);
            NSString *jsonStr = [response bodyAsString];
            NSArray *jsonArr = [jsonStr JSONValue];
            
            if (parties && jsonArr && jsonArr.count > 0) {
                parties = nil;
            }
            
            parties = [[NSMutableArray alloc] initWithCapacity:jsonArr.count];
            
            for (NSDictionary *dict in jsonArr) {
                NSDictionary *partyDict = [dict objectForKey:@"party"];
                NSLog(@"%@", partyDict);
                Party *aParty = [[Party alloc] init];
                [aParty setWithDictionary:partyDict];
                [parties addObject:aParty];
            }
            NSLog(@"%@", parties);
            [listTableView reloadData];
        }
        
    } else if ([request isPOST]) {
        
        // Handling POST /other.json        
        if ([response isJSON]) {
            NSLog(@"Got a JSON response back from our POST!");
        }
        
    } else if ([request isDELETE]) {
        
        // Handling DELETE /missing_resource.txt
        if ([response isNotFound]) {
            NSLog(@"The resource path '%@' was not found.", [request resourcePath]);
        }
    }
}


#pragma mark - Action Methods

- (IBAction)segmentedControlChanged:(id)sender {
    
    if (segmentedControl.selectedSegmentIndex == 0) {
        //show list
        mapView.hidden = YES;
        listTableView.hidden = NO;
    } else {
        //show map
        mapView.hidden = NO;
        listTableView.hidden = YES;
    }
}

- (void)addButtonPressed {
    AddPartyViewController *vc = [[AddPartyViewController alloc] initWithNibName:@"AddPartyViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
